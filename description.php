<?php

class Animal 
{
	public $name = 'Dog Chack';

	public function showName(){
		return $this->name;
	}

	static public function helloWorld(){
		return 'Hello World';
	}

	public function showNameHello(){
		return $this->name . ', ' . self::helloWorld();
	}
}

